<?php
namespace TodoBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
//use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use TodoBundle\Entity\User;


class UserCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('uip-test:todo:user')
			->setDescription('Create/edit user.')
			->setHelp('Create new user or change password, if user alrady exists.')
			->addArgument('login', InputArgument::REQUIRED, 'login')
			->addArgument('password', InputArgument::REQUIRED, 'password')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
//		$em = $this->getDoctrine()->getEntityManager();
		$em = $this->getContainer()->get('doctrine')->getEntityManager();
		$user = $em->getRepository('TodoBundle:User')->findOneByLogin($input->getArgument('login'));
		if (!$user){
			$user = new User;
			$user->setLogin($input->getArgument('login'));
			$em->persist($user);
		}
		$user->setPass(hash('sha256', $input->getArgument('password')));
		$em->flush();
		$output->writeln('uip-test:todo:user command');
	}
}
