<?php

namespace TodoBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use TodoBundle\Entity\Task;

/**
 * 1 list	GET /api/tasks.json	200, 400, 403
 * 2 create	POST /api/tasks.json	201, 400, 403
 * 3 edit	PUT /api/tasks/id.json	204, 400, 403, 404
 * 4 show	GET /api/tasks/id.json	200, 400, 403, 404
 * 5 delete	DELETE /api/tasks/id.json	204, 403, 404
 */

class DefaultController extends FOSRestController
{
	/**
	 * @Route("/tasks/{id}.json", requirements={"id": "\d+"})
	 * @Method("DELETE")
     * @ApiDoc(
     *     description="Удаляет задачу",
     *     statusCodes={
     *         204="Задача успешно удалена",
     *         401="Требуется авторизация",
     *         403="Доступ запрещён (задача принадлежит другому пользователю)",
     *         404="Задача не найдена"
     *     },
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "description"="Номер задачи"
     *         }
     *     }
     * )
	 */
	public function deleteAction(Request $request, $id)
	{
        $user=$this->checkUser($request);
        $task=$this
            ->getDoctrine()
            ->getRepository('TodoBundle:Task')
            ->find($id)
        ;
        if(!$task)
            throw new NotFoundHttpException();
        if($task->getUser()!=$user->getId())
            throw new AccessDeniedHttpException();
        $em=$this->getDoctrine()->getEntityManager();
        $em->remove($task);
        $em->flush();
		return new Response('delete');
	}
	
	/**
	 * @Route("/tasks/{id}.json", requirements={"id": "\d+"})
	 * @Method("PUT")
     * @ApiDoc(
     *     description="Редактирует выбранную задачу",
     *     statusCodes={
     *         204="Задача успешно сохранена",
     *         400={
     *             "Некорректные входные данные",
     *             "Не найдена родительская задача, или она принадлежит другому пользователю",
     *             "Родительская задача является дочерней к редактируемой (попытка создать циклическую связь)"
     *         },
     *         401="Требуется авторизация",
     *         403="Доступ запрещён (задача принадлежит другому пользователю)",
     *         404="Задача не найдена"
     *     },
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "description"="Номер задачи"
     *         },
     *         {
     *             "name"="name",
     *             "dataType"="string",
     *             "requirement"="до 255 символов",
     *             "description"="Текст задачи"
     *         },
     *         {
     *             "name"="parent",
     *             "dataType"="integer",
     *             "requirement"="Должна принадлежать этому пользователю",
     *             "description"="Номер родительской задачи"
     *         }
     *     }
     * )
	 */
	public function editAction(Request $request, $id)
	{
        $user=$this->checkUser($request);
        $task=$this
            ->getDoctrine()
            ->getRepository('TodoBundle:Task')
            ->find($id)
        ;
        if(!$task)
            throw new NotFoundHttpException();
        if($task->getUser()!=$user->getId())
            throw new AccessDeniedHttpException();
        $name=$request->request->get('name', '');
        if($name=='' or strlen($name)>255)
            throw new BadRequestHttpException();
        $parent=null;
        if($request->request->has('parent') and $request->request->get('parent')!=''){
            $parent_id=(int)$request->request->get('parent', -1);
            $parent=$this
                ->getDoctrine()
                ->getRepository('TodoBundle:Task')
                ->find($parent_id)
            ;
            if(!$parent)
                throw new BadRequestHttpException();//а может 404?
            if($parent->getUser()!=$user->getId())
                throw new BadRequestHttpException();

        }
        if($parent && $parent->getRoot()==$task->getRoot() && $parent->getLkey() >= $task->getLkey() && $parent->getRkey() <= $task->getRkey())
            throw new BadRequestHttpException();

        $task->setName($name);
        $task->setParent($parent);
        $em=$this->getDoctrine()->getEntityManager();
//        $em->persist($task);
        $em->flush();
		return new Response('', 204);
	}

	/**
	 * @Route("/tasks/{id}.json", requirements={"id": "\d+"})
	 * @Method("GET")
     * @ApiDoc(
     *     description="Отображает выбранную задачу",
     *     statusCodes={
     *         200="Успешное выполнение",
     *         401="Требуется авторизация",
     *         403="Доступ запрещён (задача принадлежит другому пользователю)",
     *         404="Задача не найдена"
     *     },
     *     requirements={
     *         {
     *             "name"="id",
     *             "dataType"="integer",
     *             "description"="Номер задачи"
     *         }
     *     }
     * )
	 */
	public function showAction(Request $request, $id)
	{
        $user=$this->checkUser($request);
        $task=$this
            ->getDoctrine()
            ->getRepository('TodoBundle:Task')
            ->find($id)
        ;
        if(!$task)
            throw new NotFoundHttpException();
        if($task->getUser()!=$user->getId())
            throw new AccessDeniedHttpException();
        return new Response($this->get('jms_serializer')->serialize($task, 'json'), 200, array('Content-Type'=>'application/json'));
	}

	/**
	 * @Route("/tasks.json")
	 * @Method("POST")
     * @ApiDoc(
     *     description="Создаёт новую задачу и привязывает к текущему пользователю",
     *     statusCodes={
     *         201="Задача успешно создана.",
     *         400="Некорректные входные параметры.",
     *         401="Требуется авторизация.",
     *         403="Нет доступа (родительская задача принадлежит другому пользователю)."
     *     },
     *     requirements={
     *         {
     *             "name"="name",
     *             "dataType"="string",
     *             "requirement"="до 255 символов",
     *             "description"="Текст задачи"
     *         },
     *         {
     *             "name"="parent",
     *             "dataType"="integer",
     *             "requirement"="Должна принадлежать этому пользователю",
     *             "description"="Номер родительской задачи"
     *         }
     *     }
     * )
	 */
	public function createAction(Request $request)
	{
        $user=$this->checkUser($request);
        $name=$request->request->get('name', '');
        if($name=='' or strlen($name)>255)
            throw new BadRequestHttpException();
        $parent=null;
        if($request->request->has('parent') and $request->request->get('parent')!=''){
            $parent_id=(int)$request->request->get('parent', -1);
            $parent=$this
                ->getDoctrine()
                ->getRepository('TodoBundle:Task')
                ->find($parent_id)
            ;
            if(!$parent)
                throw new BadRequestHttpException();//а может 404?
            if($parent->getUser()!=$user->getId())
                throw new AccessDeniedHttpException();

        }
        $task=new Task();
        $task->setUser($user->getId());
        $task->setName($name);
        if($parent) {
            $task->setParent($parent);
//            $parent->addChild($task);
        }
        $em=$this->getDoctrine()->getEntityManager();
        $em->persist($task);
        $em->flush();
		return new Response('', 201);
	}

    /**
     * @Route("/tasks.json")
     * @Method("GET")
     * @ApiDoc(
     *     description="Получает список задач для текущего пользователя",
     *     statusCodes={
     *         200="Успешное выполнение.",
     *         400="Некорректные входные данные.",
     *         401="Требуется авторизация."
     *     },
     *     requirements={
     *         {
     *             "name"="limit",
     *             "dataType"="integer",
     *             "requirement"="1 <= limit <= 100",
     *             "description"="Максимальное количество строк в ответе."
     *         },
     *         {
     *             "name"="offset",
     *             "dataType"="integer",
     *             "requirement"="0 <= offset",
     *             "description"="Смещение относительно первой строки в ответе."
     *         }
     *     }
     * )
     */
    public function listAction(Request $request)
    {
        $user=$this->checkUser($request);
        $limit=(int)$request->query->get('limit', 100);
        $offset=(int)$request->query->get('offset', 0);
        if($limit<1 or $limit>100 or $offset<0)
            throw new BadRequestHttpException();
        $tasks=$this
            ->getDoctrine()
            ->getRepository('TodoBundle:Task')
            ->findBy(array('user'=>$user->getId()), array('root'=>'ASC', 'lkey'=>'ASC'), $limit, $offset)
        ;

        $serializer = $this->get('jms_serializer');
        return new Response($serializer->serialize($tasks, 'json'), 200, array('Content-Type'=>'application/json'));
    }

    private function checkUser(Request $request)
    {
        if(!$request->server->has('PHP_AUTH_USER') or !$request->server->has('PHP_AUTH_PW'))
            throw new UnauthorizedHttpException('Basic realm=""');
        $user=$this->getDoctrine()->getRepository('TodoBundle:User')->findOneBy(array('login' => $request->server->get('PHP_AUTH_USER')));
        if(!$user or $user->getPass()!=hash('sha256',$request->server->get('PHP_AUTH_PW')))
            throw new UnauthorizedHttpException('Basic realm=""');
        return $user;
    }
}
