<?php
//id, name, parent, user, cdate, mdate
namespace TodoBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Gedmo\Tree\Entity\Repository\NestedTreeRepository")
 * @ORM\Table(name="task")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("all")
 */

class Task// implements jsonSerializable
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose
	 */
	protected $id;

    /**
     * @ORM\Column(type="integer",name="user_id")
     * @JMS\Expose
     */
    protected $user;

	/**
	 * @ORM\Column(type="string", length=255)
     * @JMS\Expose
	 */
	protected $name;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="TodoBundle\Entity\Task")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    protected $root;

	/**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="TodoBundle\Entity\Task", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
	 * ORM\Column(type="integer", nullable=true)
     * @JMS\Expose
     * @JMS\Accessor(getter="getParentId")
     * @JMS\Type("int")
	 */
	protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="TodoBundle\Entity\Task", mappedBy="parent")
     * @ORM\OrderBy({"lkey" = "ASC"})
     */
    protected $children;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     * @JMS\Expose
     */
	protected $level;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     * @JMS\Expose
     */
	protected $lkey;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     * @JMS\Expose
     */
	protected $rkey;

	/**
	 * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     * @JMS\Expose
	 */
	protected $cdate;

	/**
	 * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"})
     * @JMS\Expose
	 */
	protected $mdate;

    public function __construct()
    {
        $this->setCdate(new \DateTime());
        $this->setMdate(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setMdateValue()
    {
        $this->setMdate(new \DateTime());
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Task
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Task
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Task
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set lkey
     *
     * @param integer $lkey
     *
     * @return Task
     */
    public function setLkey($lkey)
    {
        $this->lkey = $lkey;

        return $this;
    }

    /**
     * Get lkey
     *
     * @return integer
     */
    public function getLkey()
    {
        return $this->lkey;
    }

    /**
     * Set rkey
     *
     * @param integer $rkey
     *
     * @return Task
     */
    public function setRkey($rkey)
    {
        $this->rkey = $rkey;

        return $this;
    }

    /**
     * Get rkey
     *
     * @return integer
     */
    public function getRkey()
    {
        return $this->rkey;
    }

    /**
     * Set cdate
     *
     * @param \DateTime $cdate
     *
     * @return Task
     */
    public function setCdate($cdate)
    {
        $this->cdate = $cdate;

        return $this;
    }

    /**
     * Get cdate
     *
     * @return \DateTime
     */
    public function getCdate()
    {
        return $this->cdate;
    }

    /**
     * Set mdate
     *
     * @param \DateTime $mdate
     *
     * @return Task
     */
    public function setMdate($mdate)
    {
        $this->mdate = $mdate;

        return $this;
    }

    /**
     * Get mdate
     *
     * @return \DateTime
     */
    public function getMdate()
    {
        return $this->mdate;
    }

    /**
     * Set root
     *
     * @param \TodoBundle\Entity\Task $root
     *
     * @return Task
     */
    public function setRoot(\TodoBundle\Entity\Task $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return \TodoBundle\Entity\Task
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     *
     * @return Task
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return integer
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function getParentId()
    {
        if(!$this->parent)
            return null;
        return $this->parent->getId();
    }

    /**
     * Add child
     *
     * @param \TodoBundle\Entity\Task $child
     *
     * @return Task
     */
    public function addChild(\TodoBundle\Entity\Task $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \TodoBundle\Entity\Task $child
     */
    public function removeChild(\TodoBundle\Entity\Task $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }
}
